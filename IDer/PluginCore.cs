using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;

namespace IDer
{
	public partial class PluginCore : PluginBase
	{
		ItemDescriber describer;
		bool on = false;

		private List<int> forcedList = new List<int>();

		protected override void Startup()
		{
			try
			{
				describer = new ItemDescriber();
				ServerDispatch += new EventHandler<NetworkMessageEventArgs>(PluginCore_ServerDispatch);
				CommandLineText += new EventHandler<ChatParserInterceptEventArgs>(PluginCore_CommandLineText);
				Core.CharacterFilter.LoginComplete += new EventHandler(CharacterFilter_LoginComplete);
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
		}

		void CharacterFilter_LoginComplete(object sender, EventArgs e)
		{
			on = false;
			WriteToChat("IDer Initialized. Type /id on to turn on output and /id off to turn it back off.", 27);
			WriteToChat("Type /id item to force id a item wether the output is on or off.", 27);
		}

		void PluginCore_CommandLineText(object sender, ChatParserInterceptEventArgs e)
		{
			if(e.Text.ToUpper().Contains(("/id").ToUpper()))
			{
				string cmd = e.Text.ToUpper().Replace(("/id ").ToUpper(), ""); //Get the command.
				switch (cmd)
				{
					case "OFF":
						WriteToChat("Will now stop output ID to chat.", 27);
						on = false;
						break;
					case "ON":
						WriteToChat("Will now start output ID to chat.", 27);
						on = true;
						break;
					case "ITEM":
						ForceId(Core.Actions.CurrentSelection);
						break;
				}
			}
		}

		void ForceId(int id)
		{
			if (!forcedList.Contains(id))
			{
				forcedList.Add(id);
				Core.Actions.RequestId(id);
			}
		}

		void PluginCore_ServerDispatch(object sender, NetworkMessageEventArgs e)
		{
			if (e.Message.Value<int>("event") == 0x00C9)
			{
				int objectId = e.Message.Value<int>("object");
				if (forcedList.Contains(objectId))
				{
					forcedList.Remove(objectId);
					WorldObject o = Core.WorldFilter[objectId];
					if(ItemDescriber.IsItemToID(o))
						WriteToChat(describer.GetItemString(o), 27);
					return;
				}
				if (!on)
					return;
				WorldObject wobj = Core.WorldFilter[objectId];
				if (wobj == null)
					return;
				try
				{
					if (ItemDescriber.IsItemToID(wobj))
						WriteToChat(describer.GetItemString(wobj), 27);
				}
				catch (Exception ex)
				{
					LogError(ex);
				}
			}
		}

		protected override void Shutdown()
		{
			try
			{
				ServerDispatch -= new EventHandler<NetworkMessageEventArgs>(PluginCore_ServerDispatch);
				CommandLineText -= new EventHandler<ChatParserInterceptEventArgs>(PluginCore_CommandLineText);
				Core.CharacterFilter.LoginComplete -= new EventHandler(CharacterFilter_LoginComplete);
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
		}

		private void LogError(Exception ex)
		{
			// I dont really feel the need to log any exceptions to a logfile.
			// On error, just output something to the status.
			string msg = "IDer: An error occured: " + ex.Message;
#if DEBUG
			WriteToChat(msg, 26);
#endif
			Core.Actions.AddStatusText(msg);
		}

		private void WriteToChat(string message, int color)
		{
			try
			{
				this.Host.Actions.AddChatText(message, color, 1);
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
		}


	}
}