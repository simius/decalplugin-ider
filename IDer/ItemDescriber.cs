﻿using Decal.Adapter.Wrappers;
using Decal.Adapter;
using Decal.Filters;

namespace IDer
{
	internal enum eImbueType
	{
		None = 0,
		CriticalStrike = 1,
		CripplingBlow = 2,
		ArmorRend = 4,
		SlashRend = 8,
		PierceRend = 16,
		BludgeonRend = 32,
		AcidRend = 64,
		ColdRend = 128,
		LightningRend = 256,
		FireRend = 512
	}

	internal enum eCleaveType
	{
		None = 0,
		SlashCleaving = 0x01,
		PierceCleaving = 0x02,
		BludgeCleaving = 0x04,
		ColdCleaving = 0x08,
		FireCleaving = 0x10,
		AcidCleaving = 0x20,
		LightningCleaving = 0x40,
	}

	internal enum eSetType
	{
		None = 0,
		Soldier = 13,
		Adept = 14,
		Archer = 15,
		Defender = 16,
		Tinker = 17,
		Crafter = 18,
		Hearty = 19,
		Dexterious = 20,
		Wise = 21,
		Swift = 22,
		Hardened = 23,
		Reinforced = 24,
		Interlocking = 25,
		FlameProof = 26,
		AcidProof = 27,
		ColdProof = 28,
		LightningProof = 29,
		Dedication = 30
	}

	public class ItemDescriber
	{
		FileService fileService;
		public ItemDescriber()
		{
			fileService = (Decal.Filters.FileService)CoreManager.Current.FileService;
		}
		
		public string GetItemString(WorldObject wObj)
		{
			string itemString = wObj.Name;
			byte spells = CheckForSpells(wObj);
			if (spells != 0)
			{
				switch (spells)
				{ 
					case 1:
						itemString += " (Major)";
						break;
					case 2:
						itemString += " (Epic)";
						break;
					case 3:
						itemString += " (Legendary)";
						break;
				}
			}
			if(wObj.Values(LongValueKey.NumberTimesTinkered) > 0)
				itemString += " " + wObj.Values(LongValueKey.NumberTimesTinkered) + " tinks";
			if (wObj.Values(LongValueKey.Imbued) != 0 && wObj.ObjectClass != ObjectClass.Jewelry)
				itemString += " (" + ((eImbueType)wObj.Values(LongValueKey.Imbued)).ToString() + ")";
			if (wObj.Values((LongValueKey)263) != 0)
				itemString += " (" + ((eCleaveType)wObj.Values((LongValueKey)263)).ToString() + ")";
			if (wObj.ObjectClass == ObjectClass.Armor)
				itemString += CheckArmor(wObj);
			else if (wObj.ObjectClass == ObjectClass.MeleeWeapon)
				itemString += CheckMeleeWeapon(wObj);
			else if (wObj.ObjectClass == ObjectClass.MissileWeapon)
				itemString += CheckMissileWeapon(wObj);
			else if (wObj.ObjectClass == ObjectClass.WandStaffOrb)
				itemString += CheckWand(wObj);
			else if (wObj.ObjectClass == ObjectClass.Jewelry || wObj.ObjectClass == ObjectClass.Clothing)
				itemString += CheckClothingAndJewelery(wObj);
			if (wObj.SpellCount > 0)
				itemString += ", Diff " + wObj.Values(LongValueKey.LoreRequirement);
			if (wObj.Values((LongValueKey)86) > 0)
				itemString += ", Level " + wObj.Values((LongValueKey)86) + "+";
			itemString += ", Work " + wObj.Values(LongValueKey.Workmanship);
			itemString += ", Value " + wObj.Values(LongValueKey.Value);
			itemString += ", Bu " + wObj.Values(LongValueKey.Burden);
			return itemString;	
		}

		private string CheckScroll(WorldObject wObj)
		{
			string ret = wObj.Name + ":";
			if (CoreManager.Current.CharacterFilter.SpellBook.Contains(wObj.Spell(0)))
				ret += "0";
			else
				ret += "1";
			return ret;
		}

		private string Getspells(WorldObject wObj)
		{
			string spells = "";
			for (int i = 0; i < wObj.SpellCount; i++)
			{
				spells += ", " + fileService.SpellTable.GetById(wObj.Spell(i));
			}
			return spells;
		}

		private string CheckMissileWeapon(WorldObject wObj)
		{
			double mod =  System.Math.Round((wObj.Values(DoubleValueKey.DamageBonus) * 100) - 100, 0);
			double meleD = System.Math.Round((wObj.Values(DoubleValueKey.MeleeDefenseBonus) * 100) - 100, 0);
			long damage = wObj.Values(LongValueKey.ElementalDmgBonus);
			string missileString = "";
			if (damage > 0)
				missileString += " +" + damage;
			if(mod > 0)
				missileString += " +" + mod + "%";
			if(meleD > 0)
				missileString += " +" + meleD + "%MD";
			missileString += Getspells(wObj);
			missileString += ". " + fileService.SkillTable.GetById(wObj.Values(LongValueKey.EquipSkill)).Name + " " + wObj.Values(LongValueKey.WieldReqValue) + " to wield";
			byte spells = CheckForSpells(wObj);
			return missileString;

		}

		private string CheckMeleeWeapon(WorldObject wObj)
		{
			double meleeD = System.Math.Round((wObj.Values(DoubleValueKey.MeleeDefenseBonus) * 100) - 100, 0);
			double attack = System.Math.Round((wObj.Values(DoubleValueKey.AttackBonus) * 100) - 100, 0);
			string meleeString = " " + System.Math.Round(wObj.Values(LongValueKey.MaxDamage) - (wObj.Values(LongValueKey.MaxDamage) * wObj.Values(DoubleValueKey.Variance)), 1) + "-" + wObj.Values(LongValueKey.MaxDamage);
			if (attack > 0)
				meleeString += "+" + attack + "%A";
			if (meleeD > 0)
				meleeString += "+" + meleeD + "%MD";			
			meleeString += Getspells(wObj);
			meleeString += ". " + fileService.SkillTable.GetById(wObj.Values(LongValueKey.EquipSkill)).Name + " " + wObj.Values(LongValueKey.WieldReqValue) + " to wield";
			byte spells = CheckForSpells(wObj);
			return meleeString;
		}

		private string CheckWand(WorldObject wObj)
		{
			string wandString = "";
			//Monster, Melee, Mana, Spells, War rec, Diff
			double eleDamage = wObj.Values(DoubleValueKey.ElementalDamageVersusMonsters);
			if (eleDamage > 0)
				wandString += " +" + System.Math.Round((eleDamage * 100) - 100, 0) + "% vs.M";

			double melee = System.Math.Round((wObj.Values(DoubleValueKey.MeleeDefenseBonus) * 100) - 100, 0);
			double manac = System.Math.Round(wObj.Values(DoubleValueKey.ManaCBonus) * 100, 0);
			if (melee > 0)
				wandString += " +" + melee + "%MD";
			if (manac > 0)
				wandString += " +" + manac + "% ManaC";
			wandString += Getspells(wObj);
			if (eleDamage > 0)
				wandString += " " + wObj.Values(LongValueKey.WieldReqValue) + "+ " + (wObj.Name.Contains("Nether") == true ? "Void" : "War") + " to wield";
			byte spells = CheckForSpells(wObj);
			return wandString;
		}

		private string CheckArmor(WorldObject wObj)
		{
			string armorString = "";
			if (wObj.Values((LongValueKey)265) >= 13 && wObj.Values((LongValueKey)265) <= 30)
				armorString += " (" + ((eSetType)wObj.Values((LongValueKey)265)).ToString() + ")";
			armorString += " Al " + wObj.Values(LongValueKey.ArmorLevel);
			armorString += Getspells(wObj);
			byte spells = CheckForSpells(wObj);
			return armorString;
		}

		private string CheckClothingAndJewelery(WorldObject wObj)
		{
			string jewlString = "";
			byte spells = CheckForSpells(wObj);
			jewlString += Getspells(wObj);
			return jewlString;
		}

		public byte CheckForSpells(WorldObject wObj)
		{
			for (int i = 0; i < wObj.SpellCount; i++)
			{
				string name = fileService.SpellTable.GetById(wObj.Spell(i)).Name.ToUpper();
				if (name.Contains("LEGENDARY"))
					return 3;
				else if (name.Contains("EPIC"))
					return 2;
				else if (name.Contains("MAJOR"))
					return 1;
			}
			return 0;
		}

		public static bool IsItemToID(WorldObject wObj)
		{
			if (wObj.ObjectClass == ObjectClass.Armor || wObj.ObjectClass == ObjectClass.MeleeWeapon
				|| wObj.ObjectClass == ObjectClass.MissileWeapon || wObj.ObjectClass == ObjectClass.WandStaffOrb
				|| wObj.ObjectClass == ObjectClass.Scroll || wObj.ObjectClass == ObjectClass.Jewelry ||wObj.ObjectClass == ObjectClass.Clothing)
				return true;
			return false;
		}
	}
}
